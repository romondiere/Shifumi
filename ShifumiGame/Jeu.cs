﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShifumiGame
{
    public class Jeu
    {
        private static Random random;

        public Jeu()
        {
            random = new Random();
        }

        public int Choix_Adversaire()
        {
            return random.Next(0, 3);
        }
        /// <summary>
        /// 0: égalité
        /// 1: le joueur 1 gagne
        /// 2: le joueur 2 gagne
        /// </summary>
        /// <param name="choixj1">choix du joueur 1 : 0 Pierre, 1 Feuille, 2 Sciseaux</param>
        /// <param name="choixj2"> choix du joueur 2: 0 Pierre, 1 Feuille, 2 Sciseaux</param>
        /// <returns>        \
        /// 0: égalité
        /// 1: le joueur 1 gagne
        /// 2: le joueur 2 gagne
        /// </returns>
        public int Eval(int choixj1, int choixj2)
        {
            if (choixj1 == 0 && choixj2 == 2) return 1;
            if (choixj1 == 1 && choixj2 == 0) return 1;
            if (choixj1 == 2 && choixj2 == 1) return 1;
            if (choixj2 == choixj1) return 0;
            else return 2;
        }

        public int Jouer1(int choix)
        {
            return Eval(choix, Choix_Adversaire());
        }
    }
}
