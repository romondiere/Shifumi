﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Shifumi
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            Animation(stone, 1000);
            Animation(scissors, 1000);
            Animation(paper, 1000);
            AnimateButton(jouerButton, 1000);
            NavigationPage.SetHasNavigationBar(this, false);
        }
        void Button_Clicked(object sender, EventArgs e)
        {
           Navigation.PushAsync( new GamePage());
        }

        async void Animation(View view, uint length)
        {
            bool always = true;
            while (always)
            {
                await view.RotateTo(45, length);
                await view.RotateTo(-45, length);
            }
        }

        async void AnimateButton(View view, uint length)
        {
            while (true)
            {
                await view.ScaleTo(1.1,length);
                await view.ScaleTo(1,length);
            }
        }
        /// <summary>
        /// Fonction réecrite pour éviter de revenir en arriere sur Android.
        /// </summary>
        /// <returns></returns>
        protected override bool OnBackButtonPressed()
        {
            return false;
        }
    }
}
