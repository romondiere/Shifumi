﻿using ShifumiGame;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Shifumi
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage, INotifyPropertyChanged
    {
        private int choixOrdi;

        public int ChoixOrdi
        {
            get { return choixOrdi; }
            set
            {
                choixOrdi = value;
                OnPropertyChanged();
            }
        }
        private bool animationBool;
        private Jeu Jeu;
        private ICommand button_Tapped;

        public ICommand Button_Tapped
        {
            get { return button_Tapped; }
            private set
            {
                button_Tapped = value;
                OnPropertyChanged();
            }
        }
        public GamePage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            Button_Tapped = new Command<String>(Tapped_ButtonChoix);
            animationBool = true;
            ChoixOrdi = -1;
            BindingContext = this;
            InitializeComponent();
            imageAnimation(imageBot, 200);
            Jeu = new Jeu();

        }
        async void imageAnimation(Image img, int delay)
        {
            while (animationBool)
            {
                //img.Source = "scissors.png";
                if (!animationBool) break;
                ChoixOrdi = 0;
                await Task.Delay(delay);
                //img.Source = "stone.png";
                if (!animationBool) break;
                ChoixOrdi = 1;
                await Task.Delay(delay);
                //img.Source = "paper.png";
                if (!animationBool) break;
                ChoixOrdi = 2;
                await Task.Delay(delay);
            }
        }

        async void Tapped_ButtonChoix(string choix)
        {
            Button_Tapped = null;
            animationBool = false;
            int choixj1 = Convert.ToInt32(choix);
            ChoixOrdi = Jeu.Choix_Adversaire();
            await imageBot.ScaleTo(1.4, 1500, Easing.BounceOut);
            await Task.Delay(500);
            if (Jeu.Eval(choixj1, ChoixOrdi) == 0) await Navigation.PushAsync(new ResultPage(0));
            if (Jeu.Eval(choixj1, ChoixOrdi) == 1) await Navigation.PushAsync(new ResultPage(1)); /*await DisplayAlert("Gagné !", "Bravo tu as gagné !", "OK"); */
            if (Jeu.Eval(choixj1, ChoixOrdi) == 2) await Navigation.PushAsync(new ResultPage(2)); /*await DisplayAlert("Oups ..", "Tu as perdu ..", "OK");*/
            imageBot.Scale = 1;
            Button_Tapped = new Command<String>(Tapped_ButtonChoix);
            animationBool = true;
            imageAnimation(imageBot, 200);
        }


    }
}