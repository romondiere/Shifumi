﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Shifumi
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultPage : ContentPage, INotifyPropertyChanged
    {
        private String victoryText;

        public String VictoryText
        {
            get { return victoryText; }
            set
            {
                victoryText = value;
                OnPropertyChanged();
            }
        }

        private String victoryImage;

        public String VictoryImage
        {
            get { return victoryImage; }
            set
            {
                victoryImage = value;
                OnPropertyChanged();
            }
        }


        public ResultPage(int victory)
        {
            if (victory == 0) { VictoryText = "Égalité"; VictoryImage = "equality.png"; }
            if (victory == 1) { VictoryText = "Victoire"; VictoryImage = "trophy.png"; }
            if (victory == 2) { VictoryText = "Défaite"; VictoryImage = "cancel.png"; }
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = this;
            stackResult.Scale = 0.7;
            stackResult.ScaleTo(1.0, 1500, Easing.BounceIn);
            RetournerAuMenu();
            

        }
        async Task RetournerAuMenu()
        {
            await Task.Delay(1800);
            await Navigation.PopToRootAsync();
        }
    }
}